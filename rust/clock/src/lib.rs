use std::{cmp, fmt};

pub struct Clock {
    hours: u8,
    minutes: u8,
}

impl Clock {
    pub fn new(hours: i32, minutes: i32) -> Self {
        let mut minutes_all = hours * 60 + minutes;

        while minutes_all < 0 {
            minutes_all += 1440;
        }

        let hours = ((minutes_all / 60) % 24) as u8;

        let minutes = (minutes_all % 60) as u8;

        Clock { hours, minutes }
    }

    pub fn add_minutes(self, minutes: i32) -> Self {
        Clock::new(self.hours as i32, self.minutes as i32 + minutes)
    }

    pub fn to_string(&self) -> String {
        format!("{}", self)
    }
}

impl fmt::Display for Clock {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}:{}",
            if self.hours < 10 {
                format!("0{}", self.hours)
            } else {
                format!("{}", self.hours)
            },
            if self.minutes < 10 {
                format!("0{}", self.minutes)
            } else {
                format!("{}", self.minutes)
            }
        )
    }
}

impl fmt::Debug for Clock {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}:{}", self.hours, self.minutes)
    }
}

impl cmp::PartialEq for Clock {
    fn eq(&self, other: &Clock) -> bool {
        self.hours == other.hours && self.minutes == other.minutes
    }
}
